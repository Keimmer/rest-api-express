const db = require("../models");
var bcrypt = require("bcryptjs");
const Personas = db.personas;
const Usuarios = db.usuarios;
const TipoUsuario = db.tiposUsuario;

exports.signup = (req, res) => {
    console.log(req.body)
  TipoUsuario.findOne({
    where: {
      id: 1,
    },
  }).then((tipo) => {
    Personas.findOne({
      where: {
        cedula: req.body.cedula,
      },
    })
      .then((pers) => {
        if (pers != null) {
          return res
            .status(500)
            .send({ message: "ya existe un usuario con esa cedula" });
        } else {
          Usuarios.findOne({
            where: {
              email: req.body.correo,
            },
          }).then((usuario) => {
            if (usuario != null) {
              return res
                .status(500)
                .send({
                  message: "ya existe un usuario con ese correo electrónico",
                });
            } else {
              Personas.create({
                cedula: req.body.cedula,
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                fecha_nacimiento: req.body.fecha_nacimiento,
              }).then((cp) => {
                Usuarios.create({
                  email: req.body.correo,
                  password: bcrypt.hashSync(req.body.password, 8),
                  tipo_usuario_id: tipo.id,
                  persona_id: cp.cedula,
                })
                  .then((usuario) => {
                    res.send(usuario);
                  })
                  .catch((e) => {
                    res.status(500).send({ message: "error en el servidor" });
                  });
              });
            }
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  });
};

exports.signin = (req, res) => {
  Usuarios.findOne({
    where: {
      email: req.body.correo,
    },
    include: [
        {model: TipoUsuario},
        {model: Personas}
    ]    
  }).then((user) => {
    var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) {
      return res.status(401).send({
        accessToken: null,
        message: "password incorrecto",
      });
    } else {
        res.send(user);
    }
  });
};
