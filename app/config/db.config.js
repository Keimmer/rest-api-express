require('dotenv').config()
module.exports = {
    HOST: process.env.DB_HOST,
    USER: process.env.DB_USERNAME,
    PASSWORD: process.env.DB_PASSWORD,
    DB: process.env.DB_DATABASE,
    dialect: "mysql",
    pool: {
      max: 50,
      min: 0,
      acquire: 900000000,
      idle: 900000000
    }
  };