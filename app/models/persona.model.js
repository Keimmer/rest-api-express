module.exports = (sequelize, Sequelize) => {
    const Persona = sequelize.define("persona", {
        cedula: {
            type: Sequelize.INTEGER,
            primaryKey: true,
        },
        nombres: {
            type: Sequelize.STRING
        },
        apellidos: {
            type: Sequelize.STRING
        },
        fecha_nacimiento: {
            type: Sequelize.DATE
        }
    },
        { timestamps: false }
    );
    return Persona;
};