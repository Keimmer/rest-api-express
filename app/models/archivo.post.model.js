module.exports = (sequelize, Sequelize) => {
    const Persona = sequelize.define("persona", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
    },
        { timestamps: false }
    );
    return Persona;
};