module.exports = (sequelize, Sequelize) => {
    const PostsUsuario = sequelize.define("posts_usuario", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
    },
        { timestamps: false }
    );
    return PostsUsuario;
};