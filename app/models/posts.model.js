module.exports = (sequelize, Sequelize) => {
    const Posts = sequelize.define("posts", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        titulo: {
            type: Sequelize.STRING
        },
        texto_post: {
            type: Sequelize.STRING
        },
        fecha_creado: {
            type: Sequelize.DATE
        },
        fecha_actualizado: {
            type: Sequelize.DATE
        }
    },
        { timestamps: false }
    );
    return Posts;
};