module.exports = (sequelize, Sequelize) => {
    const TipoUsuario = sequelize.define("tipo_usuario", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tipo: {
            type: Sequelize.STRING
        }
    },
        { timestamps: false }
    );
    return TipoUsuario;
};