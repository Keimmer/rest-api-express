module.exports = (sequelize, Sequelize) => {
    const Comentarios = sequelize.define("comentarios", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        comentario: {
            type: Sequelize.STRING
        },
        fehca_creado: {
            type: Sequelize.DATE
        },
    },
        { timestamps: false }
    );
    return Comentarios;
};