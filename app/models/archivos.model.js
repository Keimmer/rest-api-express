module.exports = (sequelize, Sequelize) => {
    const Persona = sequelize.define("persona", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        directorio: {
            type: Sequelize.STRING
        },
    },
        { timestamps: false }
    );
    return Persona;
};