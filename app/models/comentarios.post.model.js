module.exports = (sequelize, Sequelize) => {
    const ComentariosPost = sequelize.define("comentarios_post", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
    },
        { timestamps: false }
    );
    return ComentariosPost;
};