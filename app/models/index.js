const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: 0,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
  define: {
    //prevent sequelize from pluralizing table names
    freezeTableName: true,
  },
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;


db.personas = require("./persona.model.js")(sequelize, Sequelize);
db.tiposUsuario = require('./tipousuario.model.js')(sequelize, Sequelize);
db.usuarios = require('./usuarios.model.js')(sequelize, Sequelize);
db.posts = require('./posts.model.js')(sequelize, Sequelize);
db.postsUsuario = require('./posts.usuarios.model.js')(sequelize, Sequelize);
db.comentarios = require('./comentarios.model.js')(sequelize, Sequelize);
db.comentariosPost = require('./comentarios.post.model.js')(sequelize, Sequelize);


db.tiposUsuario.hasMany(db.usuarios, {
    foreignKey: "tipo_usuario_id",
    otherKey: "id"
});
db.usuarios.belongsTo(db.tiposUsuario, {
    foreignKey: "tipo_usuario_id",
    otherKey: "id"
});
db.personas.hasOne(db.usuarios, {
    foreignKey: "persona_id",
    otherKey: "cedula",
});
db.usuarios.belongsTo(db.personas, {
    foreignKey: "persona_id",
    otherKey: "cedula",
});
db.usuarios.belongsToMany(db.posts, {
    through: db.postsUsuario,
    foreignKey: "usuario_id"
});
db.posts.belongsToMany(db.usuarios, {
    through: db.postsUsuario,
    foreignKey: "post_id"
});

module.exports = db;
