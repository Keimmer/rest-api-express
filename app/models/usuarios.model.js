module.exports = (sequelize, Sequelize) => {
    const Usuarios = sequelize.define("usuarios", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
    },
        { timestamps: false }
    );
    return Usuarios;
};