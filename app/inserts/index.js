const db = require("../models");
const tipoUsuario = db.tiposUsuario;

async function insertTipos() {
  tipoUsuario
    .bulkCreate([
      {
        tipo: "ADMINISTRADOR",
      },
      {
        tipo: "MODERADOR",
      },
      {
        tipo: "USUARIO",
      },
    ])
    .then((result) => {})
    .catch((e) => {
      console.log(e);
    });
}

module.exports = {
  insertTipos,
};