const express = require("express");
require('dotenv').config()
var cors = require('cors');
const app = express();
const port = process.env.PORT;
var bodyParser = require("body-parser");

const db = require("./app/models");
const inserts = require("./app/inserts");

app.use(cors({
  origin: '*'
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

db.sequelize
  .sync({ force: true } /* SOLO PARA DESARROLLO */)
  .then(async () => {
    await inserts.insertTipos();
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

/* connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`personas` (`cedula` INT NOT NULL,    `nombres` VARCHAR(45) NULL,    `apellidos` VARCHAR(45) NULL,    PRIMARY KEY (`cedula`))",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`tipos_usuario` ( `id_usertype` INT NOT NULL, `tipo` VARCHAR(45) NOT NULL,    PRIMARY KEY (`id_usertype`))",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`usuarios` (`user_id` INT NOT NULL,`email` VARCHAR(45) NOT NULL,    `password` VARCHAR(45) NOT NULL,    `id_usertype` INT NOT NULL,    `persona_cedula` INT NOT NULL,    PRIMARY KEY (`user_id`),    CONSTRAINT `fk_usuarios_tipos_usuario`      FOREIGN KEY (`id_usertype`)      REFERENCES `Blog`.`tipos_usuario` (`id_usertype`)      ON DELETE NO ACTION      ON UPDATE NO ACTION,    CONSTRAINT `fk_usuarios_personas1`      FOREIGN KEY (`persona_cedula`)      REFERENCES `Blog`.`personas` (`cedula`)      ON DELETE NO ACTION      ON UPDATE NO ACTION)",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`posts` (`id_post` INT NOT NULL AUTO_INCREMENT,`texto_post` VARCHAR(45) NOT NULL,    `fecha_carga` VARCHAR(45) NULL,    `fecha_actualizacion` VARCHAR(45) NULL,    `fecha_eliminado` VARCHAR(45) NULL,    PRIMARY KEY (`id_post`))",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`archivos` (    `id_arch` INT NOT NULL AUTO_INCREMENT,    `ruta` VARCHAR(45) NOT NULL,    `tipo` VARCHAR(45) NOT NULL,    `fecha_carga` VARCHAR(45) NOT NULL,    PRIMARY KEY (`id_arch`))",
  function (error, results, fields) {
    if (error) throw error;
  }
);

connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`archivos_post` (    `id_archivos_post` INT NOT NULL AUTO_INCREMENT,    `id_post` INT NOT NULL,    `id_arch` INT NOT NULL,    PRIMARY KEY (`id_archivos_post`),    CONSTRAINT `fk_archivos_post_posts1`      FOREIGN KEY (`id_post`)      REFERENCES `Blog`.`posts` (`id_post`)      ON DELETE NO ACTION      ON UPDATE NO ACTION,    CONSTRAINT `fk_archivos_post_archivos1`      FOREIGN KEY (`id_arch`)      REFERENCES `Blog`.`archivos` (`id_arch`)      ON DELETE NO ACTION      ON UPDATE NO ACTION)",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`posts_usuario` (    `id_post_usuario` INT NOT NULL AUTO_INCREMENT,    `id_post` INT NOT NULL,    `user_id` INT NOT NULL,    PRIMARY KEY (`id_post_usuario`),    CONSTRAINT `fk_posts_usuario_posts1`      FOREIGN KEY (`id_post`)      REFERENCES `Blog`.`posts` (`id_post`)      ON DELETE NO ACTION      ON UPDATE NO ACTION,    CONSTRAINT `fk_posts_usuario_usuarios1`      FOREIGN KEY (`user_id`)      REFERENCES `Blog`.`usuarios` (`user_id`)      ON DELETE NO ACTION      ON UPDATE NO ACTION)",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`comentarios` (    `id_comentario` INT NOT NULL AUTO_INCREMENT,    `comentario` VARCHAR(45) NOT NULL,    `fecha_creado` VARCHAR(45) NULL,    `user_id` INT NOT NULL,    PRIMARY KEY (`id_comentario`),    CONSTRAINT `fk_comentarios_usuarios1`      FOREIGN KEY (`user_id`)      REFERENCES `Blog`.`usuarios` (`user_id`)      ON DELETE NO ACTION      ON UPDATE NO ACTION)",
  function (error, results, fields) {
    if (error) throw error;
  }
);

connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`comentarios_post` (    `id_comentario_post` INT NOT NULL AUTO_INCREMENT,    `id_comentario` INT NOT NULL,    `id_post` INT NOT NULL,    PRIMARY KEY (`id_comentario_post`),    CONSTRAINT `fk_comentarios_post_comentarios1`      FOREIGN KEY (`id_comentario`)      REFERENCES `Blog`.`comentarios` (`id_comentario`)      ON DELETE NO ACTION      ON UPDATE NO ACTION,    CONSTRAINT `fk_comentarios_post_posts1`      FOREIGN KEY (`id_post`)      REFERENCES `Blog`.`posts` (`id_post`)      ON DELETE NO ACTION      ON UPDATE NO ACTION)",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`tipos_reaccion` (    `id_tipo_reaccion` INT NOT NULL,    `tipo_reaccion` VARCHAR(45) NOT NULL,    PRIMARY KEY (`id_tipo_reaccion`))",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`reacciones` (    `id_reacciones` INT NOT NULL,    `id_tipo_reaccion` INT NOT NULL,    `usuarios_user_id` INT NOT NULL,    PRIMARY KEY (`id_reacciones`),    CONSTRAINT `fk_reacciones_tipos_reaccion1`      FOREIGN KEY (`id_tipo_reaccion`)      REFERENCES `Blog`.`tipos_reaccion` (`id_tipo_reaccion`)      ON DELETE NO ACTION      ON UPDATE NO ACTION,    CONSTRAINT `fk_reacciones_usuarios1`      FOREIGN KEY (`usuarios_user_id`)      REFERENCES `Blog`.`usuarios` (`user_id`)      ON DELETE NO ACTION      ON UPDATE NO ACTION)",
  function (error, results, fields) {
    if (error) throw error;
  }
);
connection.query(
  "CREATE TABLE IF NOT EXISTS `Blog`.`reacciones_post` (    `id_reacciones_post` INT NOT NULL AUTO_INCREMENT,    `id_reacciones` INT NOT NULL,    `id_post` INT NOT NULL,    PRIMARY KEY (`id_reacciones_post`),    CONSTRAINT `fk_reacciones_post_reacciones1`      FOREIGN KEY (`id_reacciones`)      REFERENCES `Blog`.`reacciones` (`id_reacciones`)      ON DELETE NO ACTION      ON UPDATE NO ACTION,    CONSTRAINT `fk_reacciones_post_posts1`      FOREIGN KEY (`id_post`)      REFERENCES `Blog`.`posts` (`id_post`)      ON DELETE NO ACTION      ON UPDATE NO ACTION)",
  function (error, results, fields) {
    if (error) throw error;
  }
);
 */

// routes
require("./app/routes/auth.routes")(app);

app.post("/crear-usuario", (req, res) => {
  console.log(req.body);
});

app.get("/hola", (req, res) => {
  res.send('hola')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
